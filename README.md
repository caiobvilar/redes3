# Resumo #

## ThreadManager e Modularidade ##

* Pensei em criar um modelo modular, deixar cada thread gerenciando algo e fazendo-as se comunicarem e processar mensagens com tarefas entre elas.

![x9BBcO9_.png](https://bitbucket.org/repo/G5REon/images/2727046630-x9BBcO9_.png)


* Dentro do ThreadManager tem uma MessageQueue que é acessada por todas as threads/modulos registrados no ThreadManager, lendo ou escrevendo mensagens e tratando-as internamente

* As ThreadMessages são compartilhadas pelas threads na MessageQueue, com a seguinte estrutura:
  - msgType: tipo da mensagem, controle, msgclient, msgserver, status etc
  - fromModule: inputhandler, serverstatehandler, connectionhandler etc
  - to: mesmos valores do fromModule
  - message: o corpo da mensagem em si
  - readCount: um contador que inicia com a quantidade de threads e a cada vez que uma thead lê, ela reduz o contador de 1 e recoloca a mensagem na MessageQueue para as outras threads lerem e caso o contador esteja em 1, quando a thread lê a mensagem e não a coloca novamente na MessageQueue

* Sei que parece meio overkill, mas isso garante que fica fácil a gente estender qualquer funcionalidade e fica fácil dividir tarefas, tipo cada um faz um módulo.Mas vcs que sabem se querem seguir essa ideia, é uma sugestão.