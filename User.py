
class User:
    def __init__(self, connection, address):
        self.conn = connection
        self.name = 0
        self.ID = address
        self.last_seen_line = 0
        self.closeConnFlag = 0

    def run(self):
        self.Welcome()

    def manual(self):
        self.conn.send(str.encode("Todos os comandos devem ser digitados da seguinte forma 'comando' + ':'"+ "\n"))
        self.conn.send(str.encode("Por exemplo, para sair, digite o comando abaixo: \n"))
        self.conn.send(str.encode("-> sair: " + "\n"))
        self.conn.send(str.encode("Todos os comandos são os listados abaixo: \n "))
        self.conn.send(str.encode("-> sair: para sair do chat \n "))
        self.conn.send(str.encode("-> nickname: para mudar o seu nick \n "))
        self.conn.send(str.encode("-> listar: para listar os usuários online no chat \n "))
        self.conn.send(str.encode("------------- Agora chame alguém para teclar! ---------- \n "))

    def Welcome(self):
        self.conn.send(str.encode("Bem vindo! Digite as informações abaixo.\n"))
        while self.name == 0:
            self.conn.send(str.encode("Escolha um nickname: "))
            data = self.conn.recv(2048)
            self.name = data.decode('utf-8')
            self.name = self.name[:(len(self.name)-2)] #this removes the \n character
            self.conn.send(str.encode("Bem vindo ao chat, "+ self.name + "!\n" ))
            self.manual()
    def SendToClient(self, message):
        self.conn.send(str.encode(message))

    def setName(self, newname):
        self.name = newname[:(len(newname)-2)] #this removes the \n character

    def closeConn(self):
        self.conn.close()