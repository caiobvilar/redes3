import socket
import sys
import User
import threading
import queue

print("[CLIENT]: iniciando o cliente...")
mainsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mainsock.connect(('localhost',5555))
pilha_msgs_in = queue.Queue()



def receive_thread():
    while True:
        print("[DEBUG]: buscando nova mensagem...")
        data = mainsock.recv(2048)
        if data:
            message = data.decode('utf-8')
            if '|' in message:
                client = message.split('|')[0]
                mesg = message.split('|')[1]
                print("[" + client +"] disse: " + msg)



def send_thread():
    while True:
        print("[DEBUG]: enviando nova mensagem...")
        msg = pilha_msgs_in.get()
        mainsock.send(msg)
        pilha_msgs_out.task_done()

def typinglistener():
    global quit
    while not quit:
        msg = input()
        if 'quit:' in msg:
            quit = True
        pilha_msgs_in.put(msg)
        pilha_msgs_in.task_done()
        
    mainsock.close()



def main(argv):
    quit = False
    t_send = threading.Thread(target=send_thread)
    t_recv = threading.Thread(target=receive_thread)

    t_send.daemon = True
    t_recv.daemon = True

    t_send.start()
    t_recv.start()

    typinglistener()


if __name__ == "__main__":
    main(sys.argv)


