import socket
import sys
import User
import threading

host = '' #you are willing to accept any
port = 5555
tLock = threading.Lock()
print("[SERVER]: iniciando o servidor...")
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #Enables TCP protocol
print("[SERVER]: socket criado.")

try:
    s.bind((host, port))
except:
    e = socket.error()
    print(str(e))


s.listen() #how many income conections we can handle
print ("[SERVER]:Aguardando nova conexão ...")

def send_thread(person):
    #   This thread will read the files from the chat.txt and seding them to the user the
    #conversation is updated
    while True:
        chat_file = open("chat.txt").readlines()
        size = len(chat_file)
        if person.last_seen_line < size:
            for i in range(person.last_seen_line,len(chat_file)):
                # Traverse the lines that this person haven't seen yet and show it!
                line = chat_file[i]
                reply = str.encode(line)
                person.conn.sendall(reply)
        person.last_seen_line = size

def receive_thread(person):
    #This thread will receive the messages from the Client and store it
    while True:
        if person.closeConnFlag:
            break
        else:
            data = person.conn.recv(2048)
        if not data:
            print ("A mensagem veio vazia. Saindo do loop.")
            break
        message = data.decode('utf-8') #Received message in STRING format
        if ':' in message:
            action = message.split(':')[0] #Split the commmand string by the '(' and grab the first element of the splitted list
            commands(person, action)
        else:
            message = ("[" + person.name + "] disse: " + message)
            tLock.acquire()
            print("O usuário " + person.name + "travou o mutex.")
            chat_file = open("chat.txt", "a") #Opens the file chat for writing in it
            chat_file.write(message) #Writes the message in STRING format
            chat_file.close() #Close the chat file
            person.last_seen_line = len(open("chat.txt").readlines())
            tLock.release() # Releases the lock
            print("O usuário " + person.name + " liberou o mutex.")
    person.closeConn()

def commands(person, action):
    actions = {
        'sair': quit,
        'listar': listing,
        'nickname': nickname
    }
    if action in actions:
        actions[action](person)
    else:
        person.SendToClient("[SERVER]:O comando "+action+ " não é reconhecido \n")

def quit(person):
    message = ("O usuário " + person.name + " saiu. ")
    updateChat(person, message) #Say to everybody that you left
    person.closeConnFlag = 1

def listing(person):
    global users
    person.SendToClient("Esse são os usuários online: \n")
    for i in range (len(users)):
        user = users[i].name
        person.SendToClient(str(i) + " - " + user + "\n")


def nickname(person):
    #This function alters the user's nickname
    oldname = person.name
    person.SendToClient("Digite o novo nickname: ")
    data = person.conn.recv(2048) #Received the data sent by the client
    newname = data.decode('utf-8') # Decode the data sent by the client
    person.setName(newname) #Set the new name
    person.SendToClient("Você agora se chama:  " + person.name + "\n")
    message = ("O usuário " + oldname + " agora se chama " + person.name + "\n")
    updateChat(person, message)

    

def updateChat(person, message):
    tLock.acquire()
    print("O usuário " + person.name + " travou o mutex")
    chat_file = open("chat.txt", "a") #Opens the file chat for writing in it
    chat_file.write(message) #Writes the message in STRING format
    chat_file.close() #Close the chat file
    person.last_seen_line = len(open("chat.txt").readlines())
    tLock.release() # Releases the lock
    print("O usuário " + person.name + " liberou o mutex.")


users = []
while True:
    conn, addr = s.accept()
    person = User.User(conn, addr)
    person.Welcome()
    users.append(person)
    print("Conectado a: " + addr[0] + ":" + str(addr[1]))
    #----- Creating the threads ------
    t_send = threading.Thread(target=send_thread, args=(person,)) # sending thread
    t_recv = threading.Thread(target=receive_thread, args=(person,)) # recevind and savind data thread

    t_send.daemon = True
    t_recv.daemon = True

    t_send.start()
    t_recv.start()
